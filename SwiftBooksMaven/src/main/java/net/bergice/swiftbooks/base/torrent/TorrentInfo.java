package net.bergice.swiftbooks.base.torrent;

public class TorrentInfo {
	public String link = "";

	public String title = "";
	public String imageURL = "";
	public String description = "";

	public int seeders = 0;
	public int leechers = 0;
	public String size = "";
	public int downloads = 0;
}
