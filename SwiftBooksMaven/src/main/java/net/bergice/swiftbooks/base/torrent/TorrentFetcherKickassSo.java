package net.bergice.swiftbooks.base.torrent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import net.bergice.swiftbooks.base.torrent.TorrentUtils.TorrentProvider;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/** Implementation of TorrentFetcher for use on kickass.so */
public class TorrentFetcherKickassSo extends Observable implements TorrentFetcherInterface {

	private int booksRead = 0; // This is used to find out what page and book index we should start reading on.
							   // It must reset when we change the book search type, filter or category.

	private int read = 0;
	/** Returns the number of bytes read in this object from URL links. */
	public int getRead() {
		return read;
	}

	public TorrentFetcherKickassSo (Observer observer) {
		if (observer!=null) {
			addObserver(observer);
		}
	}

	/** Fetches a document from the specified URL and returns it.
	 * Also counts the number of bytes read from the connection. */
	private Document getDocFromUrl (String url) {
		Connection connection = Jsoup.connect(url);
		Document doc;
		try {
			doc = connection.get();
			read += connection.response().bodyAsBytes().length;
			return doc;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("\ngetDocFromUrl returned null with url query: '" + url + "'");
		return null;
	}

	/** Returns the torrent list from kickass.so on the specified page. */
	private List<TorrentHeader> fetchTorrentList(String url) {
		Document doc = getDocFromUrl(url);
		if (doc==null) {
			System.out.println ("fetchTorrentList aborted due to doc returned was null. Most likely a socket error.");
			return null;
		}

    	Elements torrents = doc.getElementsByClass("odd");
    	Elements even = doc.getElementsByClass("even");
    	torrents.addAll(even);

    	if (torrents.size()>0) {
    		System.out.println("Found " + torrents.size() + " torrents on this page.\n");
    	} else {
    		System.out.println("Found 0 torrents!\n");
    		return null;
    	}
    	
    	List<TorrentHeader> list = new ArrayList<TorrentHeader>();

    	// Add torrents here
        for(int i=0; i<torrents.size(); i++) {
        	Element torrent = torrents.get(i);
        	
    		TorrentHeader header = new TorrentHeader();

    		// Get Torrent Title
        	Elements cellMainLink = torrent.getElementsByClass("cellMainLink");
        	for (int j=0; j<cellMainLink.size(); j++) {
        		Element a = cellMainLink.get(j);

        		header.title = a.text();
        		header.url = "https://kickass.to" + a.attr("href");
        	}
        	
        	// Get Torrent Download URL
        	Elements link = torrent.getElementsByClass("icon16");
        	for (int j=0; j<link.size(); j++) {
        		Element a = link.get(j);

        		header.download = a.attr("href");
        	}

    		list.add(header);
        }

        return list;
	}

	/** Fetches info about a specific torrent at the URL and returns it. */
	private TorrentInfo fetchTorrentInfo(String url) {
		TorrentInfo info = new TorrentInfo();

		Document doc = getDocFromUrl(url);

    	Elements desc = doc.select("#desc");
    	info.description = desc.toString();

		return info;
	}

	/** Returns the book list from amazon.com on the specified page. */
	private List<TorrentHeader> fetchAmazonList(String url) {
		System.out.println("Fetching amazon list from: '" + url + "'");

		Document doc = getDocFromUrl(url);
		if (doc==null) {
			System.out.println("getDocFromUrl returned null in fetchAmazonList");
			return null;
		}

    	Elements amazonBooks = doc.getElementsByClass("zg_itemImmersion");

    	if (amazonBooks.size()>0) {
    		System.out.println("Found " + amazonBooks.size() + " books on this page.\n");
    	} else {
    		System.out.println("Found 0 books!\n");
    		return null;
    	}
    	
    	List<TorrentHeader> list = new ArrayList<TorrentHeader>();

    	// Add torrents here
        for(int i=0; i<amazonBooks.size(); i++) {
        	Element torrent = amazonBooks.get(i);
        	
    		TorrentHeader header = new TorrentHeader();

    		// Get cover URL in high resolution.
    		String cover = torrent.getElementsByClass("zg_image").get(0).
    				getElementsByClass("zg_itemImageImmersion").get(0).
    				getElementsByTag("a").get(0).
    				getElementsByTag("img").get(0).attr("src");

    		// Format the URL so that it returns the image in high resolution.
    		int first_ = -1;
    		for (int j=0; j<cover.length(); j++) {
    			if (cover.charAt(j) == '_') {
    				first_ = j;
    				break;
    			}
    		}
    		if (first_!=-1) {
        		cover = cover.subSequence(0, first_+1) + ".jpg";
    		}
    		System.out.println("COVERURL: " + cover);
    		
    		header.cover = cover;
    		
    		// Get author
    		header.author = torrent.getElementsByClass("zg_byline").get(0).text();

    		// Get title
    		header.title = torrent.getElementsByClass("zg_title").get(0).getElementsByTag("a").get(0).text();

    		// Get torrents that match the title of the current book.
    		List<TorrentHeader> query = getTorrentSearchResults(header.title);        

    		if (query!=null) {
        		if (query.size()>0) {
        			// Fill TorrentHeader with Torrent download information.
        			header.url = query.get(0).url;
        			header.download = query.get(0).download;
        			header.torrentHeaders = query;
        			System.out.println("Found torrent search results for " + header.title + ", url = " + header.url + " with " + header.torrentHeaders.size() + " torrent links.");

            		list.add(header);            

        			setChanged();
        			notifyObservers(header);
        			
        		} else {
        			System.out.println("Found no torrent search results for " + header.title);
        		}
    		} else {
    			System.out.println("Found no torrent search results for " + header.title + " due to query returned was null");
    		}
        }
        
        return list;
	}
	
	private List<TorrentHeader> getTorrentSearchResults (String title) {
		String titleFormatted = title;
		titleFormatted = titleFormatted.replace(":", "%3A");
		titleFormatted = titleFormatted.replace(" ", "%20");
		String search = "https://kickass.to/usearch/"+titleFormatted+"%20category%3Abooks/";
		List<TorrentHeader> query = fetchTorrentList(search);
		return query;
	}

	Thread thread = new Thread() {
		public void run() {
			fetchAmazonList(TorrentUtils.getTorrentListURL(TorrentProvider.AmazonTop, 1, false));
			System.out.println("fetchAmazonList completed, ending ASync thread.");
		}
	};

	public void getTopASync(int numBooks) {
		if (!thread.isAlive()) {
			System.out.println("Starting book request thread");
			thread = new Thread() {
				public void run() {
					fetchAmazonList(TorrentUtils.getTorrentListURL(TorrentProvider.AmazonTop, 1, false));
					System.out.println("fetchAmazonList completed, ending ASync thread.");
				}
			};
			thread.start();
		} else {
			System.out.println("Books already in request!");
		}
	}

	@Override
	public List<TorrentHeader> getTop(int numBooks) {
		List<TorrentHeader> list = fetchAmazonList(TorrentUtils.getTorrentListURL(TorrentProvider.AmazonTop, 1, false));
		return list;
	}

	@Override
	public List<TorrentHeader> getNew(int numBooks) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TorrentHeader> getCategory(int numBooks, Category category, Filter filter) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TorrentHeader> getSearch(int numBooks, String search) {
		// TODO Auto-generated method stub
		return null;
	}

}
