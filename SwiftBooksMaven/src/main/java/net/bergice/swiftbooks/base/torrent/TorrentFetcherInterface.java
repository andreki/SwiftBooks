package net.bergice.swiftbooks.base.torrent;
import java.util.List;


public interface TorrentFetcherInterface {
	public int getRead ();

	public List<TorrentHeader> getNew (int numBooks);
	public List<TorrentHeader> getTop (int numBooks);
	public List<TorrentHeader> getCategory (int numBooks, Category category, Filter filter);
	public List<TorrentHeader> getSearch (int numBooks, String search);
}
