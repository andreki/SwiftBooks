package net.bergice.swiftbooks.activities;

import net.bergice.swiftbooks.SwiftBooksMaven.R;
import net.bergice.swiftbooks.services.bookservice.models.BookPreviewItem;
import net.bergice.swiftbooks.utils.DownloadTask;
import net.bergice.swiftbooks.utils.imageloader.ImageLoader;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/** Activity for viewing details about a book */
public class BookPreviewActivity extends Activity {
	
	private BookPreviewItem book;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.book_preview);
		getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setTitle("");
  	  	getActionBar().setIcon(null);
//  	  	getActionBar().setDisplayUseLogoEnabled(true);
		fillInfo ();
	}
	
	private void fillInfo() {
		Intent intent = getIntent();
		book = (BookPreviewItem) intent.getSerializableExtra("BOOK_SENT");
		if (book==null) {
			System.out.println("No book found in intent BookPreviewActivity.");
			onBackPressed();
			return;
		}

		TextView book_title = (TextView) findViewById(R.id.book_title);
		book_title.setText(book.name);

		TextView book_author = (TextView) findViewById(R.id.book_author);
		book_author.setText(book.author);

		ImageView book_cover = (ImageView) findViewById(R.id.book_cover);
		book_cover.setImageResource(book.drawableId);

        ImageLoader imgLoader = new ImageLoader(this);
        imgLoader.DisplayImage(book.cover, -1, book_cover);

		TextView torrent_url = (TextView) findViewById(R.id.torrent_url);
		torrent_url.setText(book.torrentFileURL);

		ImageButton torrent_download = (ImageButton) findViewById(R.id.torrent_download);
		torrent_download.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				onTorrentDownloadButtonClicked ();
			}
		});
	}

	protected void onTorrentDownloadButtonClicked() {
		// declare the dialog as a member field of your activity
		ProgressDialog mProgressDialog;

		// instantiate it within the onCreate method
		mProgressDialog = new ProgressDialog(BookPreviewActivity.this);
		mProgressDialog.setMessage("A message");
		mProgressDialog.setIndeterminate(true);
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		mProgressDialog.setCancelable(true);

		// execute this when the downloader must be fired
		final DownloadTask downloadTask = new DownloadTask(BookPreviewActivity.this);
		downloadTask.execute(book.torrentFileURL);

		mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
		    @Override
		    public void onCancel(DialogInterface dialog) {
		        downloadTask.cancel(true);
		    }
		});

	    onBackPressed();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    onBackPressed();
	    return true;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
//	    MenuInflater inflater = getMenuInflater();
//	    inflater.inflate(R.menu.book_view_menu, menu);

	    return super.onCreateOptionsMenu(menu);
	}
}
