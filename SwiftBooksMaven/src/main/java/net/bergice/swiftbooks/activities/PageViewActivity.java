package net.bergice.swiftbooks.activities;

import java.util.ArrayList;
import java.util.List;

import net.bergice.swiftbooks.SwiftBooksMaven.R;
import net.bergice.swiftbooks.adapters.PageAdapter;
import net.bergice.swiftbooks.fragments.BookBrowserFragment;
import net.bergice.swiftbooks.fragments.BookFinderFragment;
import net.bergice.swiftbooks.utils.Utils;
import android.app.ActionBar;
import android.app.SearchManager;
import android.content.Context;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;

/** The main activity that contains the ViewPager.
 * @author bergice */
public class PageViewActivity extends FragmentActivity {
    PageAdapter pageAdapter;

    private void setupActionBar(ActionBar actionBar) {
    	int screenSize = Utils.getScreenSize(this);
    	switch(screenSize) {
    	case Configuration.SCREENLAYOUT_SIZE_SMALL:
        	actionBar.setIcon(R.drawable.ic_launcher);
    	break;
    	case Configuration.SCREENLAYOUT_SIZE_NORMAL:
        	actionBar.setIcon(R.drawable.ic_launcher);
    	break;
    	}
    	actionBar.setTitle("");
    }

    @Override
	public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	
    	ActionBar actionBar = getActionBar();
    	setupActionBar(actionBar);

		setContentView(R.layout.view_pager);
		ViewPager pager = (ViewPager)findViewById(R.id.viewpager);
		  
		List<Fragment> fragments = getFragments();
		pageAdapter = new PageAdapter(getSupportFragmentManager(), fragments);
		pager.setAdapter(pageAdapter); 

		//Bind the title indicator to the adapter
	//	TitlePageIndicator titleIndicator = (TitlePageIndicator)findViewById(R.id.titles);
	//	titleIndicator.setViewPager(pager);
    }

	private List<Fragment> getFragments(){
		List<Fragment> fList = new ArrayList<Fragment>();

		fList.add(BookFinderFragment.newInstance()); 
		fList.add(BookBrowserFragment.newInstance());

		return fList;
    }

    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.main, menu);

	    // Associate searchable configuration with the SearchView
	    SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
	    SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
	    searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
	    searchView.setOnQueryTextListener(new OnQueryTextListener()
	    {
	        @Override
	        public boolean onQueryTextSubmit(String query) {
	        	System.out.println(query);
	        	return true;
	        }

			@Override
			public boolean onQueryTextChange(String arg0) {
				return false;
			}
	    });

	    return super.onCreateOptionsMenu(menu);
    }
}