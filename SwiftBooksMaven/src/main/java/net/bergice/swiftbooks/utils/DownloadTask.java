package net.bergice.swiftbooks.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;

import com.turn.ttorrent.client.SharedTorrent;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.PowerManager;

//usually, subclasses of AsyncTask are declared inside the activity class.
//that way, you can easily modify the UI thread from here
public class DownloadTask extends AsyncTask<String, Integer, String> {
	
	 private Context context;
	 private PowerManager.WakeLock mWakeLock;
	
	 public DownloadTask(Context context) {
	     this.context = context;
	 }
	
	 @Override
	 protected String doInBackground(String... sUrl) {
	     InputStream input = null;
	     OutputStream output = null;
	     HttpURLConnection connection = null;	         
	     
	     String path = Environment.getExternalStorageDirectory().getPath()+
        		 File.separator+"SwiftBooks"+File.separator+"cache"+File.separator+"file.torrent";
         
	     try {
	         URL url = new URL(sUrl[0]);
	         connection = (HttpURLConnection) url.openConnection();
	         connection.connect();
	
	         // expect HTTP 200 OK, so we don't mistakenly save error report
	         // instead of the file
	         if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
	             return "Server returned HTTP " + connection.getResponseCode()
	                     + " " + connection.getResponseMessage();
	         }
	
	         // this will be useful to display download percentage
	         // might be -1: server did not report the length
	         int fileLength = connection.getContentLength();
	
	         // download the file
	         input = connection.getInputStream();

	         Utils.createDirIfNotExists("SwiftBooks/cache");
	         output = new FileOutputStream(path);

	         byte data[] = new byte[4096];
	         long total = 0;
	         int count;
	         while ((count = input.read(data)) != -1) {
	             // allow canceling with back button
	             if (isCancelled()) {
	                 input.close();
	                 return null;
	             }
	             total += count;
	             // publishing the progress....
	             if (fileLength > 0) // only if total length is known
	                 publishProgress((int) (total * 100 / fileLength));
	             output.write(data, 0, count);
	         }
	     } catch (Exception e) {
	         return e.toString();
	     } finally {
	         try {
	             if (output != null)
	                 output.close();
	             if (input != null)
	                 input.close();
	         } catch (IOException ignored) {
	         }

	         if (connection != null)
	             connection.disconnect();
	     }

	     String torrentOut = Environment.getExternalStorageDirectory().getPath()+File.separator+"SwiftBooks"+File.separator+"output";
         Utils.createDirIfNotExists("SwiftBooks/output");
	     try {
			downloadTorrent (path, torrentOut);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	     
	     return null;
	 }

	private void downloadTorrent(String torrentFile, String outputDir) throws UnknownHostException, IOException {
		com.turn.ttorrent.client.Client client = new com.turn.ttorrent.client.Client(
		  InetAddress.getLocalHost(),

		  SharedTorrent.fromFile(
		    new File(torrentFile),
		    new File(outputDir)));

		client.setMaxDownloadRate(50.0);
		client.setMaxUploadRate(50.0);

		client.download();

		client.waitForCompletion();
	}
}