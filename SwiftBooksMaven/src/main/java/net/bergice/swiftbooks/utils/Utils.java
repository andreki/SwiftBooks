package net.bergice.swiftbooks.utils;

import java.io.File;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

/** Static class to perform generic operations across.
 * @author bergice */
public class Utils {

	/** Returns the screen size of the specified fragment. Compare the screen size by for example using: {@code getScreenSize(fragment) == Configuration.SCREENLAYOUT_SIZE_NORMAL;}*/
	public static int getScreenSize(Fragment fragment) {
		return fragment.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;
	}

	/** Returns the screen size of the specified fragment. Compare the screen size by for example using: {@code getScreenSize(fragment) == Configuration.SCREENLAYOUT_SIZE_NORMAL;}*/
	public static int getScreenSize(Activity fragment) {
		return fragment.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;
	}

	/** Returns the orientation of the specified fragment. Compare the orientation by for example using: {@code getOrientation(fragment) == Configuration.ORIENTATION_LANDSCAPE;}*/
	public static int getOrientation(Fragment fragment) {
		return fragment.getResources().getConfiguration().orientation;
	}

	/** Returns the display metrics data of the specified fragment. */
	public static DisplayMetrics getDisplayMetrics(Fragment fragment) {
		return fragment.getResources().getDisplayMetrics();
	}

	/** Returns true if the specified context is a tablet device. */
	public static boolean isTablet(Context context) {
	    return (context.getResources().getConfiguration().screenLayout
	            & Configuration.SCREENLAYOUT_SIZE_MASK)
	            >= Configuration.SCREENLAYOUT_SIZE_LARGE;
	}

	/** Creates a directory at the specific path if one doesn't already exist. */
	 public static boolean createDirIfNotExists(String path) {
		    boolean ret = true;

		    File file = new File(Environment.getExternalStorageDirectory(), path);
		    if (!file.exists()) {
		        if (!file.mkdirs()) {
		            Log.e("Utils", "Problem creating folder " + path + " in external storage.");
		            ret = false;
		        }
		    }
		    return ret;
		}
	    
	 /** Returns true if the specified context has a network connection. */
	    public static boolean isNetworkAvailable (Context context) {
	    	ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    	NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    	return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	    }

		 /** Sends the specified view to the back of it's parent child ordering. */
	    public static void sendViewToBack(final View child) {
	        final ViewGroup parent = (ViewGroup)child.getParent();
	        if (null != parent) {
	            parent.removeView(child);
	            parent.addView(child, 0);
	        }
	    }

		 /** Returns true if the specified activity has boolean preference 'firstrun' set to true.
		  * Sets the preference to false if it was true. */
	    public static boolean isFirstTimeRun (Activity activity) {

	        SharedPreferences prefs = null;
	        prefs = activity.getSharedPreferences("net.bergice.swiftbooks", Activity.MODE_PRIVATE);
	        if (prefs.getBoolean("firstrun", true)) {
	            prefs.edit().putBoolean("firstrun", false).commit();
	            return true;
	        }
	        return false;
	    }
}
