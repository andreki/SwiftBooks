package net.bergice.swiftbooks.adapters;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import butterknife.InjectView;

import net.bergice.swiftbooks.SwiftBooksMaven.R;
import net.bergice.swiftbooks.services.bookservice.models.BookPreviewItem;
import net.bergice.swiftbooks.utils.imageloader.ImageLoader;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

/** Adapter for filling the book library grid view */
public class BookLibraryAdapter extends ObservableAdapter
{
    private LayoutInflater inflater;
	private Context context;

    private List<BookPreviewItem> items = new ArrayList<BookPreviewItem>();

    public BookLibraryAdapter(Context context)
    {
    	this.context = context;
        inflater = LayoutInflater.from(context);

        OnCreate ();

    }

    public void requestMoreData () {
    }

	public void OnCreate () {
    	requestMoreData();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i)
    {
        return items.get(i);
    }

    @Override
    public long getItemId(int i)
    {
        return items.get(i).drawableId;
    }

    static class ViewHolder {
//      @InjectView(R.id.picture) 
      ImageView picture;
//      @InjectView(R.id.text) 
      TextView text;
//      @InjectView(R.id.text_author) 
      TextView text_author;
//      @InjectView(R.id.pages) 
      ImageView pages;
//      @InjectView(R.id.loading) 
      ProgressBar loading;

      public ViewHolder(View view) {
    	  picture = (ImageView) view.findViewById(R.id.picture);
    	  text = (TextView) view.findViewById(R.id.text);
    	  text_author = (TextView) view.findViewById(R.id.text_author);
    	  pages = (ImageView) view.findViewById(R.id.pages);
    	  loading = (ProgressBar) view.findViewById(R.id.loading);
//        ButterKnife.inject(this, view);
      }
    }
    
    @Override
    public View getView(int position, View view, ViewGroup parent)
    {
    	ViewHolder holder;
        BookPreviewItem item = (BookPreviewItem)getItem(position);
        
        // If view has not been already initialized.
        if(view == null)
        {
        	// Inflate view
           view = inflater.inflate(R.layout.single_book_item, parent, false);
           holder = new ViewHolder(view);
           view.setTag(holder);

           // Fade in view
           final Animation animationFadeIn = AnimationUtils.loadAnimation(context, R.anim.fadein);
           view.startAnimation(animationFadeIn);
        } else {
        	// Fetch view
            holder = (ViewHolder) view.getTag();
        }
   		holder.loading.setVisibility(View.GONE);
//   		holder.picture.setVisibility(View.GONE);

//   		holder.picture.setScaleType(ImageView.ScaleType.CENTER_CROP);
//        holder.pages.getLayoutParams().width = holder.picture.getWidth();

        holder.text.setText(item.name);
        holder.text_author.setText(item.author);

        // Load cover picture from URL
        if (item.cover!="") {
          ImageLoader imgLoader = new ImageLoader(context);
//          imgLoader.DisplayImage(item.cover, holder.loading, holder.picture);
          imgLoader.DisplayImage(item.cover, -1, holder.picture);
        } else {
        	holder.picture.setImageResource(item.drawableId);
        }

        return view;
    }

	@Override
	public void update(Observable arg0, Object arg1) {
		// TODO Auto-generated method stub
		
	}
}