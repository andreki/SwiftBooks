package net.bergice.swiftbooks.adapters;

import java.util.Observer;

import android.widget.BaseAdapter;

/** BaseAdapter based abstract class that implements the Observer interface and contains one method that must be overrided to know when someone notifies us that we must request more data.
 * @author bergice */
public abstract class ObservableAdapter extends BaseAdapter implements Observer {

    public abstract void requestMoreData ();

}
