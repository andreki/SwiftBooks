package net.bergice.swiftbooks.fragments;

import net.bergice.swiftbooks.SwiftBooksMaven.R;
import net.bergice.swiftbooks.adapters.BookBrowserAdapter;
import net.bergice.swiftbooks.adapters.BookLibraryAdapter;
import net.bergice.swiftbooks.services.bookservice.models.BookPreviewItem;
import net.bergice.swiftbooks.views.BookScrollerGridView;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import butterknife.ButterKnife;

/** Fragment for viewing books installed */
public class BookBrowserFragment  extends Fragment {
//	@InjectView(R.id.gridview) 
	BookScrollerGridView gridview;

	Activity activity;
	Context context;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		FragmentActivity    faActivity  = (FragmentActivity)    super.getActivity();
	    LinearLayout        llLayout    = (LinearLayout)    inflater.inflate(R.layout.book_grid_view, container, false);
	    context = llLayout.getContext();

	    ButterKnife.inject(faActivity);
	    gridview = (BookScrollerGridView) llLayout.findViewById(R.id.gridview);
		prepareGridView ();
		return llLayout;
	}

	private void prepareGridView() {
		gridview.setImageTextAdapter(new BookLibraryAdapter(context));

		gridview.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			}
		});
	}

	public static final BookBrowserFragment newInstance() {
		BookBrowserFragment f = new BookBrowserFragment();
		return f;
	}
}
