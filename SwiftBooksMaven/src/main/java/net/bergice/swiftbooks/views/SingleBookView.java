package net.bergice.swiftbooks.views;

import net.bergice.swiftbooks.SwiftBooksMaven.R;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

/** The layout that contains book views. Used in BookPreview, BookLibrary and BookFinder views. 
 * @author bergice */
public class SingleBookView extends LinearLayout {

    public SingleBookView(Context context)
    {
        super(context);
    }

    public SingleBookView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public SingleBookView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }

    /** Matches the overlay ImageView dimensions to the book cover dimensions. */
    private void measureBookCover() {

    	// Fetch Views
        FixedAspectRatioImageView picture = (FixedAspectRatioImageView) findViewById(R.id.picture);
        ImageView overlay = (ImageView) findViewById(R.id.overlay);

        // Get LayoutParams
        android.view.ViewGroup.LayoutParams overlayLayoutParams = overlay.getLayoutParams();
        
        // Update LayoutParams
        overlayLayoutParams.width = picture.getMeasuredWidth();
        overlayLayoutParams.height = picture.getMeasuredHeight();
        overlay.setLayoutParams(overlayLayoutParams);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        measureBookCover();
        setMeasuredDimension(getMeasuredWidth(), getMeasuredHeight());
    }
}
